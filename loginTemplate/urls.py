from authentication.views import AccountViewSet, LoginView, LogoutView, CheckLoggedInView
from django.conf.urls import patterns, include, url
from django.contrib import admin
from loginTemplate.views import IndexView
from rest_framework import routers

router = routers.SimpleRouter()
router.register(r'accounts', AccountViewSet)

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'loginTemplate.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^api/v1/', include(router.urls)),
    url(r'^api/v1/auth/login/$', LoginView.as_view(), name='login'),
    url(r'^api/v1/auth/logout/$', LogoutView.as_view(), name='logout'),
    url(r'^api/v1/auth/check/$', CheckLoggedInView.as_view(), name='check_login'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^.*$', IndexView.as_view())
)
