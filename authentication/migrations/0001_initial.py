# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, help_text='', auto_created=True)),
                ('password', models.CharField(verbose_name='password', max_length=128, help_text='')),
                ('last_login', models.DateTimeField(verbose_name='last login', blank=True, null=True, help_text='')),
                ('email', models.EmailField(max_length=254, unique=True, help_text='')),
                ('username', models.CharField(max_length=40, unique=True, help_text='')),
                ('first_name', models.CharField(max_length=60, blank=True, help_text='')),
                ('last_name', models.CharField(max_length=60, blank=True, help_text='')),
                ('tagline', models.CharField(max_length=140, blank=True, help_text='')),
                ('is_admin', models.BooleanField(default=False, help_text='')),
                ('created_at', models.DateTimeField(help_text='', auto_now_add=True)),
                ('updated_at', models.DateTimeField(help_text='', auto_now=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
