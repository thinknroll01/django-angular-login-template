# README #

### Summary ###

This is just a login template for Django and AngularJS framework, 
following a tutorial from [thinkstar](https://thinkster.io/django-angularjs-tutorial/).

### set up ###

install pip
```
#!bash

sudo apt-get install python pip -y
```

install django dependency

```
#!bash

sudo pip install django djangoframework
```

run

```
#!bash

python manage.py runserver
```

then open [http://127.0.0.1:8000](http://127.0.0.1:8000)
