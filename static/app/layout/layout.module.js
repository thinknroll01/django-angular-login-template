/**
 * layout modules declaration
 * @namespace app.layout
 */
(function () {
    'use strict';

    angular.module('app.layout',[
        'app.layout.controllers'
    ]);

    angular.module('app.layout.controllers', []);
})();
